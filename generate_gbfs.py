import time
import json

class GenerateGBFS():

    def serialize(self, bikes):
        data = {}
        data["ttl"] = 60
        data["last_updated"] = int(time.time())
        data["bikes"] = list(map(lambda bike: bike[1].gbfs_dict(), bikes.items()))

        return json.dumps(data)

