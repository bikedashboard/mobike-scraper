import requests
import psycopg2
import logging
import time
import queue
import threading
import generate_gbfs
import os

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)

class MobikeLocation():
    def __init__(self, bike_id, lat, lng, bike_type):
        self.bike_id = bike_id
        self.lat = lat
        self.lng = lng
        self.bike_type = bike_type
    
    def save(self, cur, measurement_id):
        cur.execute("""
        INSERT INTO  cycle_measurement
        (sample_id, bike_id, location, bike_type) VALUES (%s, %s,  ST_SetSRID(ST_MakePoint(%s, %s),4326), %s)
        """, (measurement_id, self.bike_id, self.lng, self.lat, self.bike_type))

    def gbfs_dict(self):
        data = {}
        data["bike_id"] = self.bike_id
        data["lat"] = self.lat
        data["lon"] = self.lng
        data["is_reserved"] = 0
        data["is_disabled"] = 0
        return data

class MobikeSniffer():
    def __init__(self):
        self.diff_lat = 0.005
        self.diff_lng = 0.004
        self.q = queue.Queue()
        self.num_worker_threads = 20
        self.bikes = {}

    def start_workers(self):
        for i in range(self.num_worker_threads):
            t = threading.Thread(target=self.worker)
            t.daemon = True
            t.start()

    def worker(self):
        while True:
            item = self.q.get()
            tmp_bikes = self.sample(item[0], item[1], 0)
            for cycle in tmp_bikes:
                self.bikes[cycle["distId"]] = MobikeLocation(cycle["distId"], cycle["distY"], cycle["distX"], cycle["biketype"])
            self.q.task_done()

    def snif_bikes2(self, start_lat, start_lng, end_lat, end_lng):
        self.start_lat = start_lat
        self.start_lng = start_lng
        self.end_lat = end_lat
        self.end_lng = end_lng
        self.bikes = {}
        lat = self.start_lat
        while lat > self.end_lat:
            lat = lat - self.diff_lat
            lng = self.start_lng
            while lng < self.end_lng:
                lng = lng + self.diff_lng
                self.q.put((lat, lng))

        self.q.join()  
        return self.bikes

    def sample(self, lat, lng, level):
        if level > 1:
            #logging.info("Level %s, stop recursion", level)
            return []
        url = "http://app.mobike.com/api/nearby/v4/nearbyBikeInfo?latitude=%s&longitude=%s" % (lat,lng) 
        headers = {'version': '8.7.1', 'platform': '1'}
        try:
            r = requests.get(url, headers=headers, timeout=4)
        except Exception as e:
            print(e)
            return []

        if r.status_code != 200:
            logging.warn(r.status_code)
            logging.warn(r.text)
            return []
        data = r.json()
        if data["code"] == 601:
            return []

        if "bike" not in data:
            print("Something went wrong")
            print(data)
            return []
        bikes = data["bike"]
        if len(bikes) >= 10:
            bikes += self.sample(lat + 0.0014, lng + 0.0014, level + 1)
            bikes += self.sample(lat + 0.0014, lng - 0.0014, level + 1)
            bikes += self.sample(lat - 0.0014, lng + 0.0014, level + 1)
            bikes += self.sample(lat - 0.0014, lng - 0.0014, level + 1)
        return bikes



class MobikeImporter():
    def __init__(self):
        self.sniffer = MobikeSniffer()
        self.sniffer.start_workers()
        self.areas = [
            # Delft
            #(52.030233, 4.309318, 51.983844, 4.411370), 
            # Rotterdam
            #(51.943332, 4.424605, 51.871836, 4.556362),
            # Den Haag
            (52.116144, 4.230356, 52.062890, 4.361227)
        ]


    def import_bikes(self):
        result = {}
        for area in self.areas:
            bikes = self.sniffer.snif_bikes2(area[0], area[1], area[2], area[3])
            result.update(bikes)
           
        return result

importer = MobikeImporter()
gbfs_generator = generate_gbfs.GenerateGBFS()

conn_str = "dbname=deelfietsdashboard"
if "dev" in os.environ:
    conn_str = "dbname=deelfietsdashboard2"

if "ip" in os.environ:
    conn_str += " host={} ".format(os.environ['ip'])
if "password" in os.environ:
    conn_str += " user=deelfietsdashboard password={}".format(os.environ['password'])


conn = psycopg2.connect(conn_str)
cur = conn.cursor()

while True:
    start = time.time()
    logging.info("Start import.")
    result = importer.import_bikes()
    number_of_bikes = len(result)
    logging.info("Completed import in %.2f, imported %s bikes." % ((time.time() - start), number_of_bikes))
    delta_time = 60 * 1 - (time.time() - start)

    json = gbfs_generator.serialize(result)
    stmt = """INSERT INTO raw_gbfs
        (feed, json)
        VALUES (%s, %s)
        ON CONFLICT (feed) DO UPDATE 
            SET json = excluded.json;
        """
    cur.execute(stmt, ("mobike", json))
    conn.commit()

    if delta_time > 0:
        time.sleep(delta_time)
